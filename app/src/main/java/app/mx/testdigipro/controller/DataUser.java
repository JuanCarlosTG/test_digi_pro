package app.mx.testdigipro.controller;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Map;

public class DataUser {

    public static final String IDENTIFIER = "digi_pro_";

    static public void set(String key, String value, Activity a) {
        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(a).edit();
        e.putString(IDENTIFIER + key, value);
        e.commit();
    }

    static public String get(String key, Activity a) {

        Map<String,?> data = PreferenceManager.getDefaultSharedPreferences(a).getAll();
        Object result = data.get(IDENTIFIER + key);
        return result != null ? result.toString() : "";

    }

    static public void clear(Activity a) {

        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(a).edit();
        Map<String,?> data = PreferenceManager.getDefaultSharedPreferences(a).getAll();

        for (Map.Entry<String,?> entry : data.entrySet()) {
            e.remove(entry.getKey());
        }

        e.commit();

    }

}
