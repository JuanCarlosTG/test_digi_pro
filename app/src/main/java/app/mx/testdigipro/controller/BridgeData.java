package app.mx.testdigipro.controller;

import android.app.Activity;
import android.util.Log;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import app.mx.testdigipro.model.User;
import app.mx.testdigipro.model.UserToDoList;
import app.mx.testdigipro.view.ProgressDialog;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class BridgeData {

    private ProgressDialog progress;
    private BridgeDataListener callback;
    static public ArrayList<BridgeData> instances = new ArrayList<BridgeData>();
    private Realm realm = Realm.getDefaultInstance();

    static public BridgeData login(Activity activity, String user, String password, String message, BridgeDataListener callback) {

        BridgeData bd = BridgeData.getInstance(activity, message, callback);
        if (bd != null){

            bd.login(activity, user, password);
        }

        return bd;
    }


    static public BridgeData save(Activity activity, String user, String mail, String password, String message, BridgeDataListener callback) {

        BridgeData bd = BridgeData.getInstance(activity, message, callback);
        if (bd != null){

            bd.save(user, mail, password);
        }

        return bd;
    }

    static public BridgeData saveTask(Activity activity, String user, String mail, String task, String message, BridgeDataListener callback) {

        BridgeData bd = BridgeData.getInstance(activity, message, callback);
        if (bd != null){

            bd.saveTask(user, mail, task);
        }

        return bd;
    }

    static public BridgeData editTask(Activity activity, String user, String mail, int itemId, String newDescription, String message, BridgeDataListener callback) {

        BridgeData bd = BridgeData.getInstance(activity, message, callback);
        if (bd != null){

            bd.editTask(user, mail, itemId, newDescription);
        }

        return bd;
    }


    static public BridgeData getTask(Activity activity, String user, String mail, String message, BridgeDataListener callback) {

        BridgeData bd = BridgeData.getInstance(activity, message, callback);
        if (bd != null){

            bd.getTask(user, mail);
        }

        return bd;
    }

    static public BridgeData deleteTask(Activity activity, String user, String mail, int itemId, String message, BridgeDataListener callback) {

        BridgeData bd = BridgeData.getInstance(activity, message, callback);
        if (bd != null){

            bd.deleteTask(user, mail, itemId);
        }

        return bd;
    }


    static BridgeData getInstance(Activity activity, String message, BridgeDataListener callback) {

        BridgeData bridgeData = new BridgeData();
        bridgeData.callback  = callback;

        if (message != null) bridgeData.progress = ProgressDialog.show(activity, message, true, false, null);

        BridgeData.instances.add(bridgeData);
        return bridgeData;

    }


    private void login(Activity activity, String user, String password) {

        try {

            RealmQuery<User> query = realm.where(User.class);
            query.equalTo("mail", user).or().equalTo("name", user);
            query.and().equalTo("password", password);

            User result = query.findFirst();
            if (result != null){

                DataUser.set("name", result.getName(), activity);
                DataUser.set("mail", result.getMail(), activity);

                success();
            }else {
                failure("No existe un usuario con estas credenciales");
            }


        }catch(Exception e){

            failure(e.getLocalizedMessage());
            e.printStackTrace();

        } finally {
            realm.close();
        }

    }


    private void save(String user, String mail, String password) {

        try {

            if (userExits(user, mail)){

                realm.beginTransaction();
                User u = realm.createObject(User.class);

                u.setName(user);
                u.setMail(mail);
                u.setPassword(password);

                realm.commitTransaction();
                success(user, mail);
            }else {
                failure("Ya se ha registrado un usuario con estos datos");
            }
            

        }catch(Exception e){

            failure(e.getLocalizedMessage());
            e.printStackTrace();

        } finally {
            realm.close();
        }

    }

    private boolean userExits(String user, String mail){

        RealmQuery<User> query = realm.where(User.class);
        query.equalTo("name", user);
        query.and().equalTo("mail", mail);

        RealmResults<User> result = query.findAll();
        return result.size() == 0;

    }

    private void saveTask(String user, String mail, String task){

        try {

            int id = getTodoListId(realm, UserToDoList.class).intValue();

            realm.beginTransaction();
            UserToDoList userTask = realm.createObject(UserToDoList.class);
            userTask.setDescription(task);
            Log.e("saved", id + "");
            userTask.setId(id);

            RealmQuery<User> query = realm.where(User.class);
            query.equalTo("name", user);
            query.or().equalTo("mail", mail);
            User result = query.findFirst();
            result.tasks.add(userTask);
            realm.commitTransaction();
            success(result.tasks);

        }catch(Exception e){

            failure(e.getLocalizedMessage());
            e.printStackTrace();

        } finally {
            realm.close();
        }

    }

    private void editTask(String user, String mail, int itemId, String newDescription) {

        try {

            RealmQuery<UserToDoList> query = realm.where(UserToDoList.class);
            query.equalTo("id", itemId);
            UserToDoList result = query.findFirst();

            realm.beginTransaction();
            result.setDescription(newDescription);
            realm.commitTransaction();

            RealmQuery<User> query2 = realm.where(User.class);
            query2.equalTo("name", user);
            query2.or().equalTo("mail", mail);
            User result2 = query2.findFirst();
            success(result2.tasks);


        }catch(Exception e){

            failure(e.getLocalizedMessage());
            e.printStackTrace();

        } finally {
            realm.close();
        }

    }

    private <T extends RealmObject> AtomicInteger getTodoListId(Realm realm, Class<T> any){

        RealmResults<T> results = realm.where(any).findAll();
        return (results.size() > 0) ? new AtomicInteger(results.max("id").intValue() + 1) : new AtomicInteger(0);

    }

    private void getTask(String user, String mail){

        try {

            RealmQuery<User> query = realm.where(User.class);
            query.equalTo("name", user);
            query.and().equalTo("mail", mail);
            User result = query.findFirst();
            success(result.tasks);

        }catch(Exception e){

            failure(e.getLocalizedMessage());
            e.printStackTrace();

        } finally {
            realm.close();
        }

    }

    private void deleteTask(String user, String mail, int itemId) {

        try {

            RealmQuery<UserToDoList> query = realm.where(UserToDoList.class);
            query.equalTo("id", itemId);
            UserToDoList result = query.findFirst();

            realm.beginTransaction();
            result.deleteFromRealm();
            realm.commitTransaction();

            RealmQuery<User> query2 = realm.where(User.class);
            query2.equalTo("name", user);
            query2.or().equalTo("mail", mail);
            User result2 = query2.findFirst();
            success(result2.tasks);


        }catch(Exception e){

            failure(e.getLocalizedMessage());
            e.printStackTrace();

        } finally {
            realm.close();
        }
    }

    private void failure (String error) {

        checkDialog();

        if (callback != null) {
            callback.onBridgeFailure(error);
        }
    }


    private void success (String user, String mail) {

        checkDialog();

        if (callback != null) {
            callback.onBridgeSuccess(user, mail);
        }

        BridgeData.instances.remove(this);

    }

    private void success (RealmList<UserToDoList> list) {

        checkDialog();

        if (callback != null) {
            callback.onTaskSuccess(list);
        }

        BridgeData.instances.remove(this);

    }

    private void success () {

        checkDialog();

        if (callback != null) {
            callback.onLogin();
        }

        BridgeData.instances.remove(this);

    }

    private void checkDialog() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }

    public interface BridgeDataListener {

        void onTaskSuccess(RealmList<UserToDoList> list);
        void onBridgeSuccess(String user, String mail);
        void onBridgeFailure(String error);
        void onLogin();

    }
}
