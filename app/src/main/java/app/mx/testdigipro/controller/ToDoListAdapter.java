package app.mx.testdigipro.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.mx.testdigipro.R;
import app.mx.testdigipro.model.UserToDoList;
import io.realm.Realm;
import io.realm.RealmList;

public class ToDoListAdapter extends RecyclerView.Adapter<ToDoListAdapter.TDLViewHolder> {

    RealmList<UserToDoList> arrayList;
    Realm realm = Realm.getDefaultInstance();
    private ItemListener callback;
    private Activity activity;

    @NonNull
    @Override
    public TDLViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View weekView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_list, viewGroup, false);
        return new TDLViewHolder(weekView);
    }

    @Override
    public void onBindViewHolder(@NonNull TDLViewHolder tdlViewHolder, int i) {

        String description = arrayList.get(i).getDescription();
        tdlViewHolder.txtItem.setText(description);
        tdlViewHolder.itemId = arrayList.get(i).getId();

    }

    @Override
    public int getItemCount() {
        if (arrayList == null) return 0;
        return arrayList.size();
    }

    public class TDLViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtItem;
        private int itemId;

        public TDLViewHolder(@NonNull View itemView) {
            super(itemView);
            txtItem = itemView.findViewById(R.id.item);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.title_dialog_task);
            builder.setPositiveButton(R.string.btn_edit_task, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    callback.onEdit(txtItem.getText().toString(), itemId);
                }
            });
            builder.setNegativeButton(R.string.btn_delete_task, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    callback.onDelete(itemId);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();

        }



    }

    public ToDoListAdapter(Activity a, RealmList<UserToDoList> userToDoLists, ItemListener callback){
        activity = a;
        arrayList = userToDoLists;
        this.callback = callback;
    }


    public interface ItemListener {

        void onEdit(String description, int id);
        void onDelete(int id);

    }


}
