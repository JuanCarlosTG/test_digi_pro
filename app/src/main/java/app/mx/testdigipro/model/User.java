package app.mx.testdigipro.model;

import io.realm.RealmList;
import io.realm.RealmObject;

public class User extends RealmObject {

    private String name;
    private String mail;
    private String password;
    public  RealmList<UserToDoList> tasks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public RealmList<UserToDoList> getTasks() {
        return tasks;
    }

    public void setTasks(RealmList<UserToDoList> tasks) {
        this.tasks = tasks;
    }
}
