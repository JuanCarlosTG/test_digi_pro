package app.mx.testdigipro.model;

import io.realm.RealmObject;

public class UserToDoList extends RealmObject {

    private String description;
    private int id;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
