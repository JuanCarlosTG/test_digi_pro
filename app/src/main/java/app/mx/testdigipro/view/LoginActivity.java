package app.mx.testdigipro.view;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import app.mx.testdigipro.R;
import app.mx.testdigipro.controller.BridgeData;
import app.mx.testdigipro.model.UserToDoList;
import io.realm.RealmList;

public class LoginActivity extends AppCompatActivity implements BridgeData.BridgeDataListener {

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        configureView();

    }


    private void configureView(){

        mEmailView = findViewById(R.id.email);
        mPasswordView = findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return false;
                }
                return false;
            }
        });

        Button mEmailSignInButton = findViewById(R.id.email_sign_in_button);
        Button mRegisterButton = findViewById(R.id.register_button);

        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mRegisterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });


    }

    private void register() {

        Intent registerIntent = new Intent(this, RegisterActivity.class);
        startActivity(registerIntent);
        finish();

    }


    private void attemptLogin() {

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();


        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            return;
        }

        ArrayList<String> errors = new ArrayList<>();
        if (email.length() < 1) errors.add(getString(R.string.error_invalid_email));
        if (password.length() < 6) errors.add(getString(R.string.error_invalid_password));

        if (errors.size() != 0) {
            String msg = "";
            for (String s : errors) {
                msg += "- " + s + "\n";
            }
            new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(msg.trim()).setNeutralButton(R.string.btn_close, null).show();
            return;
        }

        BridgeData.login(this, email, password, "Comprobando", this);

    }


    @Override
    public void onTaskSuccess(RealmList<UserToDoList> list) {}

    @Override
    public void onBridgeSuccess(String user, String mail) {}

    @Override
    public void onBridgeFailure(String error) {
        new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(error).setNeutralButton(R.string.btn_close, null).show();
    }

    @Override
    public void onLogin() {
        Intent intent = new Intent(this, ToDoListActivity.class);
        startActivity(intent);
        finish();
    }
}

