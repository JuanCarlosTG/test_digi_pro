package app.mx.testdigipro.view;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import app.mx.testdigipro.R;
import app.mx.testdigipro.controller.BridgeData;
import app.mx.testdigipro.controller.DataUser;
import app.mx.testdigipro.controller.ToDoListAdapter;
import app.mx.testdigipro.model.UserToDoList;
import io.realm.RealmList;

public class ToDoListActivity extends AppCompatActivity implements BridgeData.BridgeDataListener, ToDoListAdapter.ItemListener {

    private EditText mAddTask;
    private Button btnAddTask;
    private Button btnChangeUser;
    private RecyclerView mRecyclerView;
    private String user;
    private String mail;
    private boolean editionEnabled = false;
    private int selectedId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_list);

        user = DataUser.get("name", this);
        mail = DataUser.get("mail", this);

        configureView();

    }

    private void configureView(){

        mAddTask        = findViewById(R.id.add_task);
        btnAddTask      = findViewById(R.id.btn_add_task);
        btnChangeUser   = findViewById(R.id.btn_change_user);
        mRecyclerView   = findViewById(R.id.recycler_view);

        mRecyclerView.setHasFixedSize(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getBaseContext());
        mRecyclerView.setLayoutManager(layoutManager);

        mAddTask.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    makeAction();
                    return false;
                }
                return false;
            }
        });

        btnAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeAction();
            }
        });

        btnChangeUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeUser();
            }
        });

        BridgeData.getTask(this, user, mail, getString(R.string.txt_saving), this);

    }

    private void changeUser() {

        DataUser.clear(this);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();

    }

    private void makeAction(){

        if (editionEnabled){
            editTask();
        }else {
            addTask();
        }
    }


    private void addTask() {

        String task = mAddTask.getText().toString();
        if (task.isEmpty()) return;
        BridgeData.saveTask(this, user, mail, task, getString(R.string.txt_saving), this);

    }

    private void editTask() {

        String task = mAddTask.getText().toString();
        if (task.isEmpty()) return;
        BridgeData.editTask(this, user, mail, selectedId, task, getString(R.string.txt_saving), this);

    }

    @Override
    public void onBridgeSuccess(String user, String mail) {}
    @Override
    public void onLogin() {}

    @Override
    public void onTaskSuccess(RealmList<UserToDoList> list) {

        editionEnabled = false;
        selectedId = 0;
        if (list != null){

            mAddTask.setText("");
            RecyclerView.Adapter rvAdapter = new ToDoListAdapter(this, list, this);
            mRecyclerView.setAdapter(rvAdapter);

        }

    }

    @Override
    public void onBridgeFailure(String error) {

        new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(error).setNeutralButton(R.string.btn_close, null).show();

    }


    @Override
    public void onEdit(String description, int itemId) {

        mAddTask.setText(description);
        editionEnabled = true;
        selectedId = itemId;

    }

    @Override
    public void onDelete(int itemId) {
        BridgeData.deleteTask(this, user, mail, itemId, "Eliminado", this);
    }
}
