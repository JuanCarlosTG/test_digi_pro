package app.mx.testdigipro.view;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import app.mx.testdigipro.R;
import app.mx.testdigipro.controller.BridgeData;
import app.mx.testdigipro.controller.DataUser;
import app.mx.testdigipro.model.UserToDoList;
import io.realm.RealmList;

public class RegisterActivity extends AppCompatActivity implements BridgeData.BridgeDataListener {

    // UI references.
    private EditText mEmailView;
    private EditText mUserView;
    private EditText mPasswordView;
    private EditText mRepeatPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        configureView();
    }

    private void configureView(){

        mEmailView      = findViewById(R.id.email);
        mUserView       = findViewById(R.id.user);
        mPasswordView   = findViewById(R.id.password);
        mRepeatPassword = findViewById(R.id.repeat_password);

        mRepeatPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    registerUser();
                    return false;
                }
                return false;
            }
        });

        Button mRegisterButton = findViewById(R.id.register_button);
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });

    }

    private void registerUser() {

        String email            = mEmailView.getText().toString();
        String user             = mUserView.getText().toString();
        String password         = mPasswordView.getText().toString();
        String repeatPassword   = mRepeatPassword.getText().toString();

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            return;
        }
        if (TextUtils.isEmpty(email)) {
            mUserView.setError(getString(R.string.error_field_required));
            return;
        }
        if (!password.equals(repeatPassword)) {
            mPasswordView.setError(getString(R.string.error_field_password));
            return;
        }

        ArrayList<String> errors = new ArrayList<>();
        if (email.length() < 1) errors.add(getString(R.string.error_invalid_email));
        if (user.length() < 1) errors.add(getString(R.string.error_invalid_user));
        if (password.length() < 6) errors.add(getString(R.string.error_invalid_password));

        if (errors.size() != 0) {
            String msg = "";
            for (String s : errors) {
                msg += "- " + s + "\n";
            }
            new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(msg.trim()).setNeutralButton(R.string.btn_close, null).show();
            return;
        }

        BridgeData.save(this, user, email, password, getString(R.string.txt_saving), this);

    }

    @Override
    public void onTaskSuccess(RealmList<UserToDoList> list) {}
    @Override
    public void onLogin() {}

    @Override
    public void onBridgeSuccess(String user, String mail) {

        DataUser.set("name", user, this);
        DataUser.set("mail", mail, this);
        Intent toDoListIntent = new Intent(this, ToDoListActivity.class);
        startActivity(toDoListIntent);
        finish();

    }

    @Override
    public void onBridgeFailure(String error) {

        new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(error).setNeutralButton(R.string.btn_close, null).show();

    }




}
