package app.mx.testdigipro.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import app.mx.testdigipro.R;
import app.mx.testdigipro.controller.DataUser;

public class SplashActivity extends AppCompatActivity {

    private static final int SLEEP_TIME = 3000; // Tiempo para transferir del splash a la pantalla siguiente

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(SLEEP_TIME);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{

                    if (DataUser.get("user", SplashActivity.this).equals("")) {

                        Intent homeIntent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(homeIntent);

                    } else {

                        Intent loginIntent = new Intent(SplashActivity.this, ToDoListActivity.class);
                        startActivity(loginIntent);

                    }

                    finish();

                }
            }
        };

        timerThread.start();

    }
}
